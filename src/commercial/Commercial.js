import Interviews from './Interviews';
import InterviewRindra from './InterviewRindra';
import InterviewAshley from './InterviewAshley';
import InterviewLovatina from './InterviewLovatina';
import Landing from './Landing';

export { Interviews, Landing, InterviewRindra, InterviewAshley, InterviewLovatina };
