import React, { Component } from 'react';
import * as Icon from 'react-feather';

import lovatinaPortrait from '../assets/img/interviews/lovatina-square.jpg'
import fastCuriousLogo from '../assets/img/interviews/fast-curious-logo.png';

class InterviewLovatina extends Component {
  render() {
    return (
      <div>
          <section class="section-half-rounded interview">
              <div class="jumbotron section-inner left-0 rounded-right bg-primary overflow-hidden col-lg-5">
                  <img src={lovatinaPortrait} alt="Image" class="img-as-bg" />
              </div>
              <div class="container text-center text-lg-left py-5">
                  <div class="row">
                      <div class="col-lg-7 ml-auto">
                          <div class="card bg-dark shadow-lg border-0 mb-0">
                              <div class="px-5 py-6">
                                  <span class="badge badge-primary badge-pill">Témoignage</span>
                                  <h2 class="h2 lh-180 text-white font-weight-bold mt-3 mb-0">Lovatina Andriamampianina</h2>
                                  <p class="h6 text-white mb-0">Étudiante en 3ème année de Licence RH - Promotion 2020 ISCAM</p>
                              </div>
                          </div>
                          <a href="https://youtu.be/Yq-mgrpmd1w" target="_blank" class="fast-curious-preview">
                              <img src={fastCuriousLogo} />
                              <div class="fast-curious-overlay">
                                  <Icon.Youtube size="4em" /><br />
                                  <span>Voir la vidéo</span>
                              </div>
                          </a>
                      </div>
                  </div>
              </div>
          </section>
          <section class="slice interview">
              <div class="container">
                  <div class="row justify-content-center">
                      <div class="col-lg-9">
                          <p class="lh-190 text-justify">
                            Lovatina, ancienne étudiante du lycée Jules Ferry en filière Littéraire. Elle termine sa 3ème année en Licence spécialisation Gestion des Ressources Humaines.
                          </p>
                          <blockquote class="blockquote blockquote-card my-5 py-5 px-5 rounded-right bg-soft-primary">
                              <p class="lead">
                                  La première année était l’année du changement pour moi. Je n’en ai gardé que de beaux souvenirs.
                              </p>
                              <footer class="blockquote-footer">
                                  Étudiante 3ème année licence, <cite class="font-weight-600" title="Lovatina Andriamampianina">Lovatina Andriamampianina</cite>
                              </footer>
                          </blockquote>
                          <article>
                              <h5 class="h4">Pourquoi as-tu choisi la spécialisation Ressources Humaines ?</h5>
                              <p class="text-justify">
                                  Quand j’ai eu mon bac, je voulais suivre le parcours psychologie parce que je suis vraiment intéressée par l’humain. Mes parents ont décidé de m’orienter vers le côté “entreprise”. C’est pour ça qu’ils m’ont conseillé d’intégrer l’ISCAM. Avant l’ISCAM je ne savais pas du tout comment fonctionnait une entreprise. Mais peu à peu j’ai pu apprendre tout ça et j’ai découvert les ressources humaines.
                              </p>
                              <h5 class="h4">Qu’est-ce que tu fais en dehors de tes études ?</h5>
                              <p class="text-justify">
                                  En dehors de l’ISCAM, je ne fais pas grand-chose (rires). Je suis vice-présidente de l’association étudiante « Yellow ». C’est une association toute fraîche, on est une trentaine. On veut promouvoir les femmes influentes à Madagascar. Notre projet phare est d’organiser une conférence avec des femmes influentes. Par exemple, il y a Marie Christinah, qui est une éco féministe ou encore la première Dame. J’espère que les femmes auront le courage de vraiment s’affirmer. Je suis persuadée que nous pouvons faire de grandes choses.
                              </p>
                              <h5 class="h4">Après ces trois années, que retiens-tu de tes études ?</h5>
                              <p class="text-justify">
                                  La leçon que je retiens c’est qu’il ne faut pas avoir peur de faire des erreurs. Je suis une personne très indécise, introvertie, on m’appelait “coincée” parce que je n’arrivais pas à m’ouvrir aux autres. C’est le séminaire qui a tout changé, l’ISCAM m’a poussé, c’est là que j’ai pris conscience je pouvais parler en public et plus m’épanouir. C’était là que j’ai commencé vraiment à m’ouvrir et à exploiter mon potentiel et à savoir m’ouvrir aux autres. Aujourd’hui, quand une opportunité s’offre à moi, j’accepte directement parce que je ne veux pas que la peur me contrôle.
                              </p>
                              <h5 class="h4">L’année scolaire est bientôt terminée, quels sont tes projets ?</h5>
                              <p class="text-justify">
                                  Trois ans c’était très court ! Je n’arrive même pas à me rendre compte que je suis déjà en 3e année, que je vais faire ma soutenance et terminer ma Licence. Après ma Licence, je ne sais pas encore sure mais, si j’ai de la chance j’irai continuer mon Master à l’étranger, au Canada.
                              </p>
                            </article>
                            <br />
                            <a href="/interviews" class="btn btn-xl btn-warning center"><Icon.MousePointer size="1em" />&nbsp;&nbsp;Lire d'autres témoignages</a>
                      </div>
                  </div>
              </div>
          </section>
      </div>
    );
  }
}

export default InterviewLovatina;
