import React, { Component } from 'react';
import * as Icon from 'react-feather';

import ashleyPortrait from '../assets/img/interviews/ashley-square.jpg'
import fastCuriousLogo from '../assets/img/interviews/fast-curious-logo.png';

class InterviewAshley extends Component {
  render() {
    return (
      <div>
          <section class="section-half-rounded interview">
              <div class="jumbotron section-inner left-0 rounded-right bg-primary overflow-hidden col-lg-5">
                  <img src={ashleyPortrait} alt="Image" class="img-as-bg" />
              </div>
              <div class="container text-center text-lg-left py-5">
                  <div class="row">
                      <div class="col-lg-7 ml-auto">
                          <div class="card bg-dark shadow-lg border-0 mb-0">
                              <div class="px-5 py-6">
                                  <span class="badge badge-primary badge-pill">Témoignage</span>
                                  <h2 class="h2 lh-180 text-white font-weight-bold mt-3 mb-0">Ashley Mamodaly</h2>
                                  <p class="h6 text-white mb-0">Étudiante 1ère année licence - Promotion 2022 ISCAM</p>
                              </div>
                          </div>
                          <a href="https://www.youtube.com/watch?v=7SoPFhwFOcg" target="_blank" class="fast-curious-preview">
                              <img src={fastCuriousLogo} />
                              <div class="fast-curious-overlay">
                                  <Icon.Youtube size="4em" /><br />
                                  <span>Voir la vidéo</span>
                              </div>
                          </a>
                      </div>
                  </div>
              </div>
          </section>
          <section class="slice interview">
              <div class="container">
                  <div class="row justify-content-center">
                      <div class="col-lg-9">
                          <p class="lh-190 text-justify">
                              Ashley Mamodaly est en 1ère année à l’Iscam Business school, promotion 2022. Étudiante à Houssen Memorial School à Tuléar, à ses 17 ans elle décide de poursuivre ses études au sein de l’Iscam Business School
                          </p>
                          <blockquote class="blockquote blockquote-card my-5 py-5 px-5 rounded-right bg-soft-primary">
                              <p class="lead">
                                  Il faut savoir saisir les opportunités
                              </p>
                              <footer class="blockquote-footer">
                                  Étudiante 1ère année licence, <cite class="font-weight-600" title="Ashley Mamodaly">Ashley Mamodaly</cite>
                              </footer>
                          </blockquote>
                          <article>
                              <h5 class="h4">Comment se passe ta première année ?</h5>
                              <p class="text-justify">
                                  Ma première année à l’Iscam se passe super bien, l’ambiance est vraiment sympa. Le séminaire a été très utile pour créer des liens entre les camarades de classe. Les étudiants s’entraident entre eux et on travaille régulièrement en groupe. La famille ISCAM est spécialement centrée sur le réseautage, elle pourra t’aider et être un tremplin pour ton futur.
                                  Les enseignants sont sympas et à l’écoute. Ils sont faciles à aborder.
                              </p>
                              <h5 class="h4">Comment s’est passé le séminaire d’intégration ?</h5>
                              <p class="text-justify">
                                  Au début du séminaire d’intégration, tout le monde avait peur à cause des échos de nos aînés. Et puis nous y sommes allés, et finalement ce fut une très belle expérience. En gros, il y avait plusieurs activités auxquelles il fallait participer. Elles servent à raffermir l’esprit d’équipe et à apprendre à travailler ensemble. Au début il y avait des querelles entre les membres du groupe, parce qu’on ne se connaissait pas. Mais au fur et à mesure on a commencé à tisser des liens et nous sommes devenus très proches, même plus que je ne l’aurais imaginé ! Nous aurions bien aimé remporter la fameuse médaille des premiers, mais nous sommes arrivés juste derrière en deuxième place.
                              </p>
                              <h5 class="h4">Où est-ce que tu déjeunes après tes cours ?</h5>
                              <p class="text-justify">
                                  Je mange un peu partout. L’avantage d’être à l’ISCAM c’est qu’il y a des restaurants partout autour. Avec mes amis nous aimons beaucoup faire des découvertes, nous n’allons jamais au même endroit.<br /><br />
                                  Il nous arrive de déjeuner à Capitale, ce n’est pas très loin. Je conseille leurs frites au fromage, c’est vraiment bon ! Je recommande aussi E-Squad, ils ont le Wi-Fi, donc c’est très pratique si tu veux travailler là-bas entre deux cours. Ils ont des chaises, ils ont des fauteuils. En fait tous les Iscamiens, la plupart du temps ils sont là avec leur téléphone (rires).
                              </p>
                              <h5 class="h4">Est-ce que tu as un petit message pour les futurs étudiants ?</h5>
                              <p class="text-justify">
                                  Savoir profiter des opportunités. Si je trouve une opportunité de partir en échange académique, je saisirai l’opportunité. Lorsque les portes s’ouvrent, il faut savoir en profiter parce qu’après elles se referment.<br /><br />
                                  Je pense aussi qu’il est bien de mentionner que l’influence familiale compte énormément. Il est important de s’entourer de personnes qui savent te booster parce que personnellement, je n’aurai pas vraiment choisi commerce toute seule. C’est ce qui te permet d’aller loin parce que tout seul en fait tu ne vas pas vraiment quelque part.<br /><br />
                                  C’est d’ailleurs l’ISCAM qui nous a appris que le réseautage c’est important. Tout seul tu peux aller vite mais quand tu seras là-haut, qu’est-ce que tu vas faire tout seul ?
                              </p>
                            </article>
                            <br />
                            <a href="/interviews" class="btn btn-xl btn-warning center"><Icon.MousePointer size="1em" />&nbsp;&nbsp;Lire d'autres témoignages</a>
                      </div>
                  </div>
              </div>
          </section>
      </div>
    );
  }
}

export default InterviewAshley;
