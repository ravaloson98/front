import React, { Component } from 'react';
import * as Icon from 'react-feather';
import digitalSchool from '../assets/img/landing/digital-school.png';

import partnerGrenoble from '../assets/img/partnerships/grenoble-school.svg';
import partnerSciencespo from '../assets/img/partnerships/sciencespo-school.svg';
import partnerHeci from '../assets/img/partnerships/heci-school.svg';
import partnerEscem from '../assets/img/partnerships/escem-school.svg';
import partner3A from '../assets/img/partnerships/3A-school.svg';
import partnerUns from '../assets/img/partnerships/uns-school.svg';
import partnerUqam from '../assets/img/partnerships/uqam-school.svg';
import partnerEsca from '../assets/img/partnerships/esca-school.svg';
import partnerEssec from '../assets/img/partnerships/essec-school.svg';
import partnerBrawijaya from '../assets/img/partnerships/brawijaya-school.svg';

import intervieweeRindra from '../assets/img/interviews/rindra-square.jpg';
import intervieweeAshley from '../assets/img/interviews/ashley-square.jpg';

import imageInternational from '../assets/img/landing/digital-school.png';

import pdfInfoGeneral from '../assets/pdf/ISCAM_Plaquette_Presentation.pdf';
import pdfInfoLicence from '../assets/pdf/ISCAM_Plaquette_Licence.pdf';
import pdfInfoMaster from '../assets/pdf/ISCAM_Plaquette_Master.pdf';

class Landing extends Component {
  render() {
    return (
      <div>
          <section class="slice slice-lg bg-primary">
              <div data-offset-top="#navbar-main">
                  <div class="container d-flex align-items-center">
                      <div class="col px-0">
                          <div class="row row-grid align-items-center">
                              <div class="col-lg-6 text-center text-lg-left">
                                  <h1 class="display-4 text-white my-4">
                                      Un programme de haut niveau
                                  </h1>
                                  <p class="lead text-white text-justify">
                                      Cette année, l’ISCAM Business School fête ses 30 ans. Nous offrons des programmes de qualité qui ont permis la mise en place de plusieurs politiques d’échanges d’étudiants accompagnés d’interventions régulières de professionnels d’entreprises et d’expériences uniques.
                                  </p>
                                  <div class="row mt-5">
                                      <div class="col-xl-5 col-lg-5 col-sm-5">
                                          <a href="#digital" class="btn btn-white btn-lg btn-icon">
                                              <span class="btn-inner--text">En savoir plus</span>
                                          </a>
                                      </div>
                                  </div>
                              </div>
                              <div class="col-lg-6 col-xl-5 col-sm-8 ml-auto mr-auto mr-lg-0">
                                  <div class="card rounded-bottom-right">
                                      <div class="card-body">
                                          <div class="d-flex align-items-center mb-4">
                                              <div>
                                                  <h5 class="h6 mb-0">INSCRIPTION AU CONCOURS</h5>
                                              </div>
                                          </div>
                                          <p>L'inscription au concours se déroule en 4 étapes :</p>
                                          <ul class="list">
                                              <li>Renseignez les informations personnelles demandées</li>
                                              <li>Téléchargez sur la plateforme les éléments de votre dossier</li>
                                              <li>Sélectionnez la période de concours souhaitée</li>
                                              <li>Finalisez votre inscription</li>
                                          </ul>
                                          <p class="text-justify">Le concours se déroulera en trois étapes. Vous serez évalué sur votre dossier académique, puis sur approbation passerez l'épreuve écrite. Sur résultat concluant, vous recevrez une convocation pour un entretien oral.</p>
                                          <div data-toggle="tooltip" data-placement="bottom" title="Inscription à partir du Lundi 22 Juin">
                                              <a class="btn btn-primary btn-icon text-white" disabled><Icon.User size="1em" />&nbsp;&nbsp;S'inscrire au concours</a>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
              <div class="shape-container shape-position-bottom shape-line">
                  <svg width="2560px" height="100px" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" preserveAspectRatio="none" x="0px" y="0px" viewBox="0 0 2560 100" style={{enableBackground:'new 0 0 2560 100'}} xmlSpace="preserve" class="">
                      <polygon points="2560 0 2560 100 0 100"></polygon>
                  </svg>
              </div>
          </section>
          <section class="slice pt-0">
              <div id="digital" class="container">
                  <div class="section-process-step">
                      <div class="container">
                          <div class="row row-grid align-items-center justify-content-between">
                              <div class="col-xl-5 col-lg-6 order-lg-2">
                                  <div class="pr-md-4">
                                      <span class="badge badge-warning badge-pill">Digital</span>
                                      <h3 class="h2 mt-4">Une école digitalisée</h3>
                                      <ul class="digital-list">
                                        <li><Icon.Airplay color="#036cb5" size="1em" /> Dématérialisation des contenus de cours</li>
                                        <li><Icon.Mail color="#036cb5" size="1em" /> Adresse email professionnelle pour chaque étudiant (septembre 2020)</li>
                                        <li><Icon.UploadCloud color="#036cb5" size="1em" /> Accès au Cloud (espace de stockage), et aux outils collaboratifs</li>
                                      </ul>
                                      <a href={pdfInfoGeneral} target="_blank" class="btn btn-warning mt-5">
                                          Télécharger la plaquette
                                      </a>
                                  </div>
                              </div>
                              <div class="col-lg-6 order-lg-1">
                                  <div class="position-relative pt-md-5 pr-md-5 pt-lg-5 pt-xl-7 pr-xl-5">
                                      <img src={digitalSchool} />
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
                  <div class="section-process-step">
                      <div class="container">
                          <div class="row row-grid align-items-center justify-content-between">
                              <div class="col-xl-5 col-lg-6">
                                  <div class="pr-md-4">
                                      <span class="badge badge-success badge-pill">Ecole reconnue</span>
                                      <h3 class="h2 mt-4 lh-190">Un rayonnement national et international</h3>
                                      <div class="mt-5">
                                          <p>La confiance des professionnels dans les recrutements. <br />
                                            Plus de 70% des diplômés recrutés dans les 3 mois après l’obtention du diplôme; les Iscamiens sont les premiers à être recrutés par les entreprises selon l’étude de FTHM en 2017.
                                          </p>
                                      </div>
                                      <a href={pdfInfoGeneral} target="_blank" class="btn btn-success mt-5">
                                          Télécharger la plaquette
                                      </a>
                                  </div>
                              </div>
                              <div class="col-lg-6">
                                  <div class="card border-0 shadow-lg">
                                      <figure>
                                          <img alt="Un rayonnement international" src={imageInternational} class="card-img" />
                                      </figure>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
                  <div class="section-process-step">
                      <div class="container">
                          <div class="row row-grid align-items-center justify-content-between">
                              <div class="col-xl-5 col-lg-6 order-lg-2">
                                  <div class="pr-md-4">
                                      <span class="badge badge-primary badge-pill">Témoignages</span>
                                      <h3 class="h2 mt-4">Retrouvez les témoignages des ISCAMiens</h3>
                                      <p class="lead my-4 lh-190">
                                          ISCAM est une famille. L'école se veut proche de ses étudiants, anciens ou actuels. Chaque personne est unique.
                                      </p>
                                      <a href="/interviews" class="link-underline-primary mt-4">Voir les interviews</a>
                                  </div>
                              </div>
                              <div class="col-xl-6 col-lg-6 order-lg-1">
                                  <div class="row mx-n2">
                                      <div class="col-sm-6 mt-sm-6 px-sm-2">
                                          <a href="/interviews/rindra">
                                              <div class="card mb-3">
                                                  <div class="h-100">
                                                      <img class="card-img-top" src={intervieweeRindra} alt="Rindra Razafindrazaka" />
                                                  </div>
                                                  <div class="card-body p-3">
                                                      <div class="d-flex align-items-center">
                                                          <div class="pl-3">
                                                              <span class="h6 text-sm mb-0">Rindra Razafindrazaka<br />Fondateur Hamac</span>
                                                          </div>
                                                      </div>
                                                  </div>
                                              </div>
                                          </a>
                                      </div>
                                      <div class="col-sm-6 mt-sm-6 px-sm-2">
                                        <a href="/interviews/ashley">
                                              <div class="card mb-3">
                                                  <div class="h-100">
                                                      <img class="card-img-top" src={intervieweeAshley} alt="Card image cap" />
                                                  </div>
                                                  <div class="card-body p-3">
                                                      <div class="d-flex align-items-center">
                                                          <div class="pl-3">
                                                              <span class="h6 text-sm mb-0">Ashley Mamodaly<br />Étudiante ISCAM</span>
                                                          </div>
                                                      </div>
                                                  </div>
                                              </div>
                                          </a>
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
                  <div id="cursus" class="section-process-step">
                      <div class="container">
                          <div class="row row-grid align-items-center justify-content-between">
                              <div class=" col-lg-6">
                                  <div class="pr-md-4">
                                      <span class="badge badge-warning badge-pill">Cursus</span>
                                      <h3 class="h2 mt-4">Découvrez nos cursus</h3>
                                  </div>
                              </div>
                          </div>
                      </div>
                      <div class="jumbotron jumbotron-licence rounded-diagonal-right bg-dark border-0 rounded-lg py-5 mt-5">
                          <div class="card-body px-5">
                              <div class="row align-items-center">
                                  <div class="col-md-8">
                                      <h4 class="text-white">Licence</h4>
                                      <p class="text-white text-justify">
                                          Le programme de Licence est dédié aux étudiants ayant obtenu leur Baccalauréat et souhaitant se spécialiser à un domaine spécifique. Les Diplômés en Licence peuvent devenir des cadres intermédiaires directement opérationnels. Ils devront être capables d’occuper des postes avec des responsabilités. Leur spécialisation et parcours permettront d’apporter une valeur ajoutée à l’entreprise une fois sur terrain.
                                      </p>
                                  </div>
                                  <div class="col-12 col-md-4 text-right">
                                      <a href={pdfInfoLicence} target="_blank" class="btn btn-white btn-icon">
                                          <span class="btn-inner--text">En savoir plus</span>
                                          <span class="btn-inner--icon">
                                              <Icon.ArrowRight size="1em" />
                                          </span>
                                      </a>
                                  </div>
                              </div>
                          </div>
                      </div>
                      <div class="jumbotron jumbotron-master rounded-diagonal-right bg-dark border-0 rounded-lg py-5 mt-5">
                          <div class="card-body px-5">
                              <div class="row align-items-center">
                                  <div class="col-md-8">
                                      <h4 class="text-white">Master</h4>
                                      <p class="text-white text-justify">
                                          Le programme Master est dédié aux personnes ayant obtenu un diplôme équivalent à la Licence (BAC+3) et souhaitant poursuivre leur cursus universitaire. Dépendant de la spécialisation, les Diplômés en Master pourront être des cadres supérieurs au sein leur entreprise, créateurs et développeur d’entreprises ou responsable de projets de recherche et de développement.
                                      </p>
                                  </div>
                                  <div class="col-12 col-md-4 text-right">
                                      <a href={pdfInfoMaster} target="_blank" class="btn btn-white btn-icon">
                                          <span class="btn-inner--text">En savoir plus</span>
                                          <span class="btn-inner--icon">
                                              <Icon.ArrowRight size="1em" />
                                          </span>
                                      </a>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </section>
          <section id="partnerships" class="slice slice-lg bg-section-secondary">
              <div class="container">
                  <div class="row mb-6">
                      <div class="col-md-6">
                          <span class="badge badge-primary badge-pill">Partenaires</span>
                          <h5 class="h5 lh-180 mt-4 mb-xl-6">Ecole à dimension internationale, découvrez nos partenaires. Un échange à l'étranger ? Pourquoi pas vous !</h5>
                      </div>
                  </div>
              </div>
              <div class="container-fluid">
                  <div class="row justify-content-xl-end">
                    <div class="col-xl-2 col-lg-4 col-sm-6">
                          <a href="https://www.grenoble-em.com/" target="_blank">
                              <div class="card hover-shadow-lg hover-translate-y-n3">
                                  <div class="pt-4 h-100 w-50 mx-auto">
                                      <img alt="Grenoble Ecole de Management" src={partnerGrenoble} class="img-fluid partner-logo" />
                                  </div>
                                  <div class="card-body pb-4 text-center">
                                      <h5 style={{wordBreak: 'normal'}} class="mb-1">Grenoble Ecole de Management</h5>
                                      <p class="text-muted text-sm mb-0">
                                          Grenoble, France
                                      </p>
                                  </div>
                              </div>
                          </a>
                      </div>
                      <div class="col-xl-2 col-lg-4 col-sm-6">
                          <a href="http://www.sciencespo-toulouse.fr/" target="_blank">
                              <div class="card hover-shadow-lg hover-translate-y-n3 mt-xl-n6">
                                  <div class="pt-4 h-100 w-50 mx-auto">
                                      <img alt="SciencesPo Toulouse" src={partnerSciencespo} class="img-fluid partner-logo" />
                                  </div>
                                  <div class="card-body pb-4 text-center">
                                      <h5 class="mb-1">Sciences Po Toulouse</h5>
                                      <p class="text-muted text-sm mb-0">
                                          Toulouse, France
                                      </p>
                                  </div>
                              </div>
                          </a>
                      </div>
                      <div class="col-xl-2 col-lg-4 col-sm-6">
                          <a href="https://www.lasalle-emci.com/" target="_blank">
                              <div class="card hover-shadow-lg hover-translate-y-n3">
                                  <div class="pt-4 h-100 w-50 mx-auto">
                                      <img alt="HECI" src={partnerHeci} class="img-fluid partner-logo" />
                                  </div>
                                  <div class="card-body pb-4 text-center">
                                      <h5 class="mb-1">HECI</h5>
                                      <p class="text-muted text-sm mb-0">
                                          Saint-Etienne, France
                                      </p>
                                  </div>
                              </div>
                          </a>
                      </div>
                      <div class="col-xl-2 col-lg-4 col-sm-6">
                          <a href="https://www.escem.fr/" target="_blank">
                              <div class="card hover-shadow-lg hover-translate-y-n3 mt-xl-n6">
                                  <div class="pt-4 h-100 w-50 mx-auto">
                                      <img alt="ESCEM" src={partnerEscem} class="img-fluid partner-logo" />
                                  </div>
                                  <div class="card-body pb-4 text-center">
                                      <h5 class="mb-1">ESCEM</h5>
                                      <p class="text-muted text-sm mb-0">
                                          Tours, France
                                      </p>
                                  </div>
                              </div>
                          </a>
                      </div>
                      <div class="col-xl-2 col-lg-4 col-sm-6">
                      </div>
                  </div>
              </div>
              <div class="container-fluid">
                  <div class="row justify-content-xl-end">
                      <div class="col-xl-2 col-lg-4 col-sm-6">
                          <a href="https://uns.ac.id/en" target="_blank">
                              <div class="card hover-shadow-lg hover-translate-y-n3 mt-xl-n10">
                                  <div class="pt-4 h-100 w-50 mx-auto">
                                      <img alt="Sebelas Maret University" src={partnerUns} class="img-fluid partner-logo" />
                                  </div>
                                  <div class="card-body pb-4 text-center">
                                      <h5 class="mb-1">Sebelas Maret University</h5>
                                      <p class="text-muted text-sm mb-0">
                                          Surakarta, Indonésie
                                      </p>
                                  </div>
                              </div>
                          </a>
                      </div>
                      <div class="col-xl-2 col-lg-4 col-sm-6">
                          <a href="https://www.ecole3a.edu/" target="_blank">
                              <div class="card hover-shadow-lg hover-translate-y-n3">
                                  <div class="pt-4 h-100 w-50 mx-auto">
                                      <img alt="3A Lyon" src={partner3A} class="img-fluid partner-logo" />
                                  </div>
                                  <div class="card-body pb-4 text-center">
                                      <h5 class="mb-1">3A Lyon</h5>
                                      <p class="text-muted text-sm mb-0">
                                          Lyon, France
                                      </p>
                                  </div>
                              </div>
                          </a>
                      </div>
                      <div class="col-xl-2 col-lg-4 col-sm-6">
                          <a href="https://uqam.ca/" target="_blank">
                              <div class="card hover-shadow-lg hover-translate-y-n3 mt-xl-n6">
                                  <div class="pt-4 h-100 w-50 mx-auto">
                                      <img alt="UQAM" src={partnerUqam} class="img-fluid partner-logo" />
                                  </div>
                                  <div class="card-body pb-4 text-center">
                                      <h5 class="mb-1">UQAM</h5>
                                      <p class="text-muted text-sm mb-0">
                                          Montréal, Québec
                                      </p>
                                  </div>
                              </div>
                          </a>
                      </div>
                      <div class="col-xl-2 col-lg-4 col-sm-6">
                          <a href="https://www.esca.ma/" target="_blank">
                              <div class="card hover-shadow-lg hover-translate-y-n3">
                                  <div class="pt-4 h-100 w-50 mx-auto">
                                      <img alt="ESCA" src={partnerEsca} class="img-fluid partner-logo" />
                                  </div>
                                  <div class="card-body pb-4 text-center">
                                      <h5 class="mb-1">ESCA Casablanca</h5>
                                      <p class="text-muted text-sm mb-0">
                                          Casablanca, Maroc
                                      </p>
                                  </div>
                              </div>
                          </a>
                      </div>
                      <div class="col-xl-2 col-lg-4 col-sm-6">
                          <a href="https://essec-douala.cm/" target="_blank">
                              <div class="card hover-shadow-lg hover-translate-y-n3 mt-xl-n6">
                                  <div class="pt-4 h-100 w-50 mx-auto">
                                      <img alt="ESSEC Douala" src={partnerEssec} class="img-fluid partner-logo" />
                                  </div>
                                  <div class="card-body pb-4 text-center">
                                      <h5 class="mb-1">ESSEC Douala</h5>
                                      <p class="text-muted text-sm mb-0">
                                          Douala, Cameroun
                                      </p>
                                  </div>
                              </div>
                          </a>
                      </div>
                      <div class="col-xl-2 col-lg-4 col-sm-6">
                          <a href="https://ub.ac.id/" target="_blank">
                              <div class="card hover-shadow-lg hover-translate-y-n3 mt-xl-n10">
                                  <div class="pt-4 h-100 w-50 mx-auto">
                                      <img alt="Université de Brawijaya" src={partnerBrawijaya} class="img-fluid partner-logo" />
                                  </div>
                                  <div class="card-body pb-4 text-center">
                                      <h5 class="mb-1">Université de Brawijaya</h5>
                                      <p class="text-muted text-sm mb-0">
                                          Brawijaya, Indonésie
                                      </p>
                                  </div>
                              </div>
                          </a>
                      </div>
                  </div>
              </div>
              <div class="shape-container shape-line shape-position-bottom">
                  <svg width="2560px" height="100px" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" preserveAspectRatio="none" x="0px" y="0px" viewBox="0 0 2560 100" style={{enableBackground:'new 0 0 2560 100'}} xmlSpace="preserve" class="">
                      <polygon points="2560 0 2560 100 0 100"></polygon>
                  </svg>
              </div>
          </section>
          <section id="faq" class="slice slice-lg">
              <div class="container">
                  <div class="row mb-5 justify-content-center text-center">
                      <div class="col-lg-8 col-md-10">
                          <h2 class=" mt-4">Questions & Réponses Fréquentes</h2>
                          <div class="mt-2">
                              <p class="lead lh-180">Consultez notre FAQ pour obtenir les réponses à vos questions.</p>
                          </div>
                      </div>
                  </div>
                  <div class="row">
                      <div class="col-xl-6">
                          <div id="accordion-1" class="accordion accordion-spaced">
                              <div class="card">
                                  <div class="card-header py-4" id="heading-1-1" data-toggle="collapse" role="button" data-target="#collapse-1-1" aria-expanded="false" aria-controls="collapse-1-1">
                                      <h6 class="mb-0"><Icon.Compass size="1em" class="mr-3" />Quelles sont les filières proposées à l’ISCAM</h6>
                                  </div>
                                  <div id="collapse-1-1" class="collapse" aria-labelledby="heading-1-1" data-parent="#accordion-1">
                                      <div class="card-body">
                                          <p class="text-justify">
                                            Nous proposons plusieurs spécialisations en Formation Initiale (FI) et Formation Continue Diplômante (FCD). La FI concerne les bacheliers souhaitant poursuivre leurs études en Licence ou en Master. Le parcours FCD concerne les professionnels souhaitant développer leurs capacités et évoluer dans leur domaine d’expertise.<br />
                                            Pour plus d’informations, veuillez consulter <a href="http://www.iscam.mg/programmes">la page explicative sur le site de l'ISCAM.</a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header py-4" id="heading-1-2" data-toggle="collapse" role="button" data-target="#collapse-1-2" aria-expanded="false" aria-controls="collapse-1-2">
                                    <h6 class="mb-0"><Icon.Key size="1em" class="mr-3" />Quelle est la procédure d’inscription au concours ?</h6>
                                </div>
                                <div id="collapse-1-2" class="collapse" aria-labelledby="heading-1-2" data-parent="#accordion-1">
                                    <div class="card-body">
                                      <p class="text-justify">
                                          L’inscription au concours se fait sur le site inscription.iscam.mg. Vous y renseignerez les informations et documents demandés. Plusieurs sessions de concours seront organisées afin de recruter les nouveaux entrants.<br />
                                          Nous vous invitons à consulter la liste des documents à fournir ainsi que le déroulement du du concours sur <a href="#cursus">les plaquettes de chaque cursus</a>.
                                      </p>
                                      </div>
                                  </div>
                              </div>
                              <div class="card">
                                  <div class="card-header py-4" id="heading-1-3" data-toggle="collapse" role="button" data-target="#collapse-1-3" aria-expanded="false" aria-controls="collapse-1-3">
                                      <h6 class="mb-0"><Icon.Folder size="1em" class="mr-3" />Quels sont les documents requis à l’inscription au concours ?</h6>
                                  </div>
                                  <div id="collapse-1-3" class="collapse" aria-labelledby="heading-1-3" data-parent="#accordion-1">
                                      <div class="card-body">
                                          <p class="text-justify">
                                              Les documents suivants doivent être téléchargés sur la plateforme d’inscription au concours :
                                          </p>
                                          <ul>
                                              <li>Pièce d’identité</li>
                                              <li>Notes des 3 dernières années d’études</li>
                                              <li>CV</li>
                                              <li>Photo d’identité</li>
                                              <li>Attestation d’emploi ou certificat de travail pour les candidats en FCD</li>
                                              <li>Copie légalisée du diplôme du bacc et autres diplômes (si disponible)</li>
                                              <li>Bordereau de versement des frais de concours à la banque</li>
                                          </ul>
                                      </div>
                                  </div>
                              </div>
                              <div class="card">
                                  <div class="card-header py-4" id="heading-1-4" data-toggle="collapse" role="button" data-target="#collapse-1-4" aria-expanded="false" aria-controls="collapse-1-4">
                                      <h6 class="mb-0"><Icon.HelpCircle size="1em" class="mr-3" />Comment se déroule le concours ?</h6>
                                  </div>
                                  <div id="collapse-1-4" class="collapse" aria-labelledby="heading-1-4" data-parent="#accordion-2">
                                      <div class="card-body">
                                          <p class="text-justify">
                                              Le concours se déroulera en trois étapes distinctes, la réussite de chaque épreuve sera déterminante pour l’admission à l’ISCAM. Toute la procédure d’admission se fera à distance. Vous serez évalué sur l’excellence de votre dossier académique. Le dossier concluant, vous passerez une épreuve écrite et un entretien oral.
                                          </p>
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>
                      <div class="col-xl-6">
                          <div id="accordion-2" class="accordion accordion-spaced">
                              <div class="card">
                                  <div class="card-header py-4" id="heading-2-1" data-toggle="collapse" role="button" data-target="#collapse-2-1" aria-expanded="false" aria-controls="collapse-2-1">
                                      <h6 class="mb-0"><Icon.Calendar size="1em" class="mr-3" />Quelles sont les prochaines dates de concours ?</h6>
                                  </div>
                                  <div id="collapse-2-1" class="collapse" aria-labelledby="heading-2-1" data-parent="#accordion-1">
                                      <div class="card-body">
                                          <p class="text-justify">
                                              Les inscriptions au concours commencent à partir du Lundi 22 Juin pour les personnes souhaitant intégrer la L1, L2 ou M2.
                                              Les concours se dérouleront en plusieurs sessions dépendant du niveau désiré :
                                          </p>
                                          <ul>
                                              <li>L1 : 09-10 juillet, 26-27 août, 24-25 septembre 2020</li>
                                              <li>L2 : 09-10 juillet, 26-27 août 2020</li>
                                              <li>M1 : 20-21 juillet 2020, 20-21 août 2020</li>
                                          </ul>
                                      </div>
                                  </div>
                              </div>
                              <div class="card">
                                  <div class="card-header py-4" id="heading-2-2" data-toggle="collapse" role="button" data-target="#collapse-2-2" aria-expanded="false" aria-controls="collapse-2-2">
                                    <h6 class="mb-0"><Icon.DollarSign size="1em" class="mr-3" />Proposez-vous des bourses d’études, comment les obtenir ?</h6>
                                  </div>
                                  <div id="collapse-2-2" class="collapse" aria-labelledby="heading-2-2" data-parent="#accordion-2">
                                      <div class="card-body">
                                          <p class="text-justify">
                                                L’ISCAM propose deux bourses : la bourse d’études pour les étudiants de la L1 jusqu’à la L3 et une bourse d’excellence de la L3 à la M2. Ces dernières sont considérées selon l’excellence du dossier scolaire, les caractéristiques sociales et l’implication dans la vie de l’établissement.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header py-4" id="heading-2-3" data-toggle="collapse" role="button" data-target="#collapse-2-3" aria-expanded="false" aria-controls="collapse-2-3">
                                      <h6 class="mb-0"><Icon.Send size="1em" class="mr-3" />Je souhaite partir en échange</h6>
                                    </div>
                                    <div id="collapse-2-3" class="collapse" aria-labelledby="heading-2-3" data-parent="#accordion-1">
                                        <div class="card-body">
                                            <p class="text-justify">
                                                Tous les étudiants de l’ISCAM peuvent bénéficier du programme d’échange à l’étranger. Ce dernier est accessible à partir de la 3ème année de Licence jusqu’à la dernière année de Master durant la période de Janvier à Juin pour une durée de 6 mois.
                                                Pour consulter la liste de nos partenaires universitaires, <a href="#partnerships">veuillez consulter la partie partenaires</a>.
                                            </p>
                                      </div>
                                  </div>
                              </div>
                             <div class="card">
                                  <div class="card-header py-4" id="heading-2-4" data-toggle="collapse" role="button" data-target="#collapse-2-4" aria-expanded="false" aria-controls="collapse-2-4">
                                      <h6 class="mb-0"><Icon.Briefcase size="1em" class="mr-3" />Avez-vous des formations dédiées aux professionnels ?</h6>
                                  </div>
                                  <div id="collapse-2-4" class="collapse" aria-labelledby="heading-2-4" data-parent="#accordion-2">
                                      <div class="card-body">
                                          <p class="text-justify">
                                              Nous proposons des formations pour les professionnels en cours du soir. Notre parcours de Formation Continue Diplômante concerne les personnes souhaitant développer leurs performances en entreprise et évoluer dans leur domaine d’expertise.<br />
                                              Pour plus d’informations concernant la FCD veuillez nous contacter sur <a href="mailto:information@iscam.mg">information@iscam.mg</a>.
                                          </p>
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </section>
      </div>
    );
  }
}

export default Landing;
