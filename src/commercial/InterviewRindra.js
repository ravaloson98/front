import React, { Component } from 'react';
import * as Icon from 'react-feather';

import rindraPortrait from '../assets/img/interviews/rindra-square.jpg'
import fastCuriousLogo from '../assets/img/interviews/fast-curious-logo.png';

class InterviewRindra extends Component {
  render() {
    return (
      <div>
          <section class="section-half-rounded interview">
              <div class="jumbotron section-inner left-0 rounded-right bg-primary overflow-hidden col-lg-5">
                  <img src={rindraPortrait} alt="Image" class="img-as-bg" />
              </div>
              <div class="container text-center text-lg-left py-5">
                  <div class="row">
                      <div class="col-lg-7 ml-auto">
                          <div class="card bg-dark shadow-lg border-0 mb-0">
                              <div class="px-5 py-6">
                                  <span class="badge badge-primary badge-pill">Témoignage</span>
                                  <h2 class="h2 lh-180 text-white font-weight-bold mt-3 mb-0">Rindra Razafindrazaka</h2>
                                  <p class="h6 text-white mb-0">Fondateur d'Hamac - Promotion 2010 ISCAM</p>
                              </div>
                          </div>
                          <a href="https://www.youtube.com/watch?v=LAAwmEp7oUo" target="_blank" class="fast-curious-preview">
                              <img src={fastCuriousLogo} />
                              <div class="fast-curious-overlay">
                                  <Icon.Youtube size="4em" /><br />
                                  <span>Voir la vidéo</span>
                              </div>
                          </a>
                      </div>
                  </div>
              </div>
          </section>
          <section class="slice interview">
              <div class="container">
                  <div class="row justify-content-center">
                      <div class="col-lg-9">
                          <p class="lh-190 text-justify">
                              Rindra Razafindrazaka, sorti de la promotion FANAMBY en 2010. Il termine sa Licence en Marketing et Communication au sein de l’ISCAM. À la fin de ses études, il crée avec 3 autres iscamiens “Hamac” une agence de Marketing, aussi régie publicitaire spécialisée dans l’exploitation et la valorisation des infrastructures urbaines et des transports en commun à Madagascar.
                          </p>
                          <blockquote class="blockquote blockquote-card my-5 py-5 px-5 rounded-right bg-soft-primary">
                              <p class="lead">
                                  Ne soyez pas attirés par les phénomènes de mode, mais trouvez votre vocation et foncez !
                              </p>
                              <footer class="blockquote-footer">
                                  Fondateur de Hamac, <cite class="font-weight-600" title="Rindra Razafindrazaka">Rindra Razafindrazaka</cite>
                              </footer>
                          </blockquote>
                          <article>
                              <h5 class="h4">Est-ce que tu peux nous décrire ton entreprise ?</h5>
                              <p class="text-justify">
                                  Hamac est une entreprise de marketing, et également une régie publicitaire. Nous faisons l’installation d’écrans publicitaires dans les bus, la mise en place des autocollants publicitaires ou la valorisation d’espaces publics à Tana.<br /><br />
                                  Il y a 7 ans de cela, nous n’étions que 4 étudiants de l’ISCAM à travailler sur le projet et aujourd’hui nous sommes près d'une dizaine. Ma plus grande fierté est le “Taxiboky”, notre premier produit. C’est un guide avec toutes les lignes de bus de Tana. Récemment, nous l’avons modélisé en application, on le nomme actuellement « City Guide ». C’est une application qui te permet de connaître la ligne de taxibe à prendre suivant ton itinéraire.
                              </p>
                              <h5 class="h4">Après ta Licence, comment s’est passée ta vie d’ISCAMien ?</h5>
                              <p class="text-justify">
                                  Je n’ai pas continué mon Master à l’ISCAM. J’ai fait ce choix personnel dans l’objectif de découvrir un autre milieu. J’ai continué à Ankatso pour le Master dans cette optique de pouvoir “découvrir” 2 mondes différents. Tout cela a été bénéfique d’un point de vue personnel mais aussi professionnel.<br /><br />
                                  Aujourd’hui, il m'arrive très souvent de rencontrer des anciens de l’ISCAM quand je discute avec d’autres responsables d’entreprise ou des partenaires. Il existe déjà quelques évènements d’anciens mais je serais ravi de rencontrer plus de membres du réseau.
                              </p>
                              <h5 class="h4">Quelles sont les difficultés que vous avez rencontrées ?</h5>
                              <p class="text-justify">
                                  Les 3 premières années étaient vraiment très difficiles. Nous avons rencontré beaucoup de difficultés et on pensait même parfois laisser tomber le projet. On travaillait encore jusque là à temps partiel pour financer une partie du projet. Mais comme nous étions solidaires, on a décidé d’affronter ça ensemble. Nous nous sommes chacun focalisés sur Hamac et y avons investi tout notre temps.<br /><br />
                                  On se déplaçait à pieds pour aller aux rendez-vous avec les clients et économiser de l’argent. Une fois arrivés dans les lieux, on devait faire en sorte d’être un peu plus présentables et nettoyer notre sueur. Je pense que la vie d’un entrepreneur ne se limite pas aux success story que l’on entend, il y a les difficultés et les galères qui vont avec.
                              </p>
                              <h5 class="h4">Est-ce que tu as des conseils à donner aux futurs étudiants de l’ISCAM ?</h5>
                              <p class="text-justify">
                                  Je me considère comme une personne curieuse, j’aime voir comment c’est fait ailleurs. Et bizarrement on partageait cette même philosophie et cela nous a permis de nous différencier des autres agences et en même temps pouvoir proposer des idées novatrices. Je pense que chacun doit apprendre à être curieux.<br /><br />
                                  Ensuite, tout le monde n’est pas fait pour être entrepreneur, c’est une vocation. Pour beaucoup de personnes être entrepreneur rime avec “réussite” et cela dévalorise l’entrepreneuriat. Être entrepreneur n’est pas facile, c’est quelque chose qui se sent.<br /><br />
                                  Ne soyez pas attirés par les phénomènes de mode, mais trouvez votre vocation et foncez ! Faites ce que vous aimez et faites le bien. Un entrepreneur doit être persévérant et intègre, je pense que sans ces qualités, les idées ne peuvent être concrétisées.
                              </p>
                            </article>
                            <br />
                            <a href="/interviews" class="btn btn-xl btn-warning center"><Icon.MousePointer size="1em" />&nbsp;&nbsp;Lire d'autres témoignages</a>
                      </div>
                  </div>
              </div>
          </section>
      </div>
    );
  }
}

export default InterviewRindra;
