import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useRouteMatch,
  useParams
} from 'react-router-dom';
import { Header, HeaderInterview, Footer } from './components/Components';
import { Landing, Interviews, InterviewRindra, InterviewAshley, InterviewLovatina } from './commercial/Commercial';
import { Register } from './platform/Platform';
import './assets/scss/quick-website.scss';
import $ from "jquery";
import bootstrap from "bootstrap";
import svgInjector from "svg-injector";
import inView from "in-view";
import imagesloaded from "imagesloaded";

function App() {
  function findGetParameter(parameterName) {
    var result = null,
        tmp = [];

    window.location.search
      .substr(1)
      .split("&")
      .forEach(function (item) {
        tmp = item.split("=");
        if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
    });

    return result;
  }

  if (findGetParameter('apiKey')) {
    window.apiKey = findGetParameter('apiKey');
  }

  return (
    <Router>
      <Switch>
        <Route path="/register/:step(1|2|3|4)" component={(routerProps) => <Register step={routerProps.match.params.step}/>} />
        <Route path="/:prefix(interviews/|)rindra">
          <HeaderInterview />
          <InterviewRindra />
          <Footer />
        </Route>
        <Route path="/:prefix(interviews/|)ashley">
          <HeaderInterview />
          <InterviewAshley />
          <Footer />
        </Route>
        <Route path="/interviews/lovatina">
          <HeaderInterview />
          <InterviewLovatina />
          <Footer />
        </Route>
        <Route path="/interviews">
          <Header />
          <Interviews />
          <Footer />
        </Route>
        <Route path="/">
          <Header />
          <Landing />
          <Footer />
        </Route>
      </Switch>
    </Router>
  );
}

export default App;
