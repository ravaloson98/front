import Header from './Header';
import HeaderInterview from './HeaderInterview';
import Footer from './Footer';

import Person from './Person';
import PersonForm from './PersonForm';
import EditPerson from './EditPerson';

import School from './School';
import SchoolForm from './SchoolForm';
import EditSchool from './EditSchool';

export { Header, HeaderInterview, Footer, Person, PersonForm, EditPerson, School, SchoolForm, EditSchool };
