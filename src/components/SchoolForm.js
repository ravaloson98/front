import React, { Component } from 'react';
import { Formik, Field, Form, ErrorMessage } from 'formik';
import * as Icon from 'react-feather';
import _ from 'lodash';
import API from '../Api.js';

class SchoolForm extends Component {
  constructor(props) {
    super(props);

    this.model = props.model;
    this.collection = props.collection;
    this.index = props.index;

    if (this.model) {
      this.state = this.model;
      this.index = this.index;
    } else {
      this.state = {
        'school_name' : '',
        'place': '',
        'level': '',
        'year': '',
      };
    }
  }

  render() {
    return (
      <div class="poi-edit mb-3">
        <h5 class="h5 mt-5">Edition d'une école <button class="btn btn-xs btn-warning float-right" onClick={this.collection.cancelAddSchool}><Icon.X size="1em" />&nbsp;Annuler</button></h5>
        <Formik
          initialValues={{
            name: '',
            place: '',
            grade: '',
            year: '',
            phone: '',
          }}
          onSubmit={(values, { setSubmitting, setFieldError, setStatus }) => {
           /**
            * API.post('/register/step-2', values)
              .then((function(response) {
                var pois = this.props.collection.state.pois;
                values.id = response.data[0];
                pois.push(values);
                this.props.collection.setState({'pois': pois});
                this.props.collection.cancelAddPoi();
              }).bind(this))
              .catch((function(error) {
                _.forOwn(error.response.data[0], function(value, key) {
                  setFieldError(_.split(key, '.')[1], value);
                });

              }).bind({ setFieldError: setFieldError}))
            ;
            **/

            this.collection.saveSchool(this.state, this.index);
            this.collection.cancelAddSchool();
          }}
        >
          <Form>
            <div class="form-group">
                <label class="form-control-label">Nom de l'école</label>
                <div class="input-group">
                    <Field
                      name="name"
                      as="input"
                      className="form-control"
                      placeholder="ex. LFT"
                    />
                </div>
                <ErrorMessage name="name" render={msg => <div class="text-danger"><Icon.XOctagon size="1em" />&nbsp;{msg}</div>} />
            </div>
            <div class="form-group">
                <label class="form-control-label">Lieu</label>
                <div class="input-group">
                    <Field
                      name="place"
                      as="input"
                      className="form-control"
                      placeholder="ex. Antananarivo"
                    />
                </div>
                <ErrorMessage name="place" render={msg => <div class="text-danger"><Icon.XOctagon size="1em" />&nbsp;{msg}</div>} />
            </div>
            <div class="form-group">
                <label class="form-control-label">Classe</label>
                <div class="input-group">
                    <Field
                      name="grade"
                      as="input"
                      className="form-control"
                      placeholder="ex. Terminale"
                    />
                </div>
                <ErrorMessage name="grade" render={msg => <div class="text-danger"><Icon.XOctagon size="1em" />&nbsp;{msg}</div>} />
            </div>
            <div class="form-group">
                <label class="form-control-label">Année</label>
                <div class="input-group">
                    <Field
                      name="year"
                      as="input"
                      className="form-control"
                        placeholder="ex. 2012"
                      />
                </div>
                <ErrorMessage name="year" render={msg => <div class="text-danger"><Icon.XOctagon size="1em" />&nbsp;{msg}</div>} />
            </div>
            <button class="btn btn-primary btn-block">Enregistrer</button>
          </Form>
        </Formik>
      </div>
    );
  }
}

export default SchoolForm;
