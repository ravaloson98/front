import React, { Component } from 'react';
import { Formik, Field, Form, ErrorMessage } from 'formik';
import * as Icon from 'react-feather';
import _ from 'lodash';
import API from '../Api.js';

class PersonForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      'jobTypeOptions': [],
      'jobTitleOptions': [],
      'companyActivityOptions': []
    };

    this.populateJobTypes = this.populateJobTypes.bind(this);
    this.populateJobTitles = this.populateJobTitles.bind(this);
    this.populateCompanyActivities= this.populateCompanyActivities.bind(this);

    this.populateJobTypes();
    this.populateJobTitles();
    this.populateCompanyActivities();
  }

  populateJobTypes() {
    API.get('/list/job_types')
      .then((function (response) {
        var jobTypeOptions = [];

        if (Array.isArray(response.data)) {
          response.data.forEach(function(value, index) {
            jobTypeOptions.push(<option value={value.id}>{value.name}</option>);
          });
        }

        this.setState({'jobTypeOptions' : jobTypeOptions});
      }).bind(this));
  }

  populateJobTitles() {
    API.get('/list/job_titles')
      .then((function (response) {
        var jobTitleOptions = [];

        if (Array.isArray(response.data)) {
          response.data.forEach(function(value, index) {
            jobTitleOptions.push(<option value={value.id}>{value.name}</option>);
          });
        }

        this.setState({'jobTitleOptions' : jobTitleOptions});
      }).bind(this));
  }

  populateCompanyActivities() {
    API.get('/list/company_activities')
      .then((function (response) {
        var companyActivityOptions = [];

        if (Array.isArray(response.data)) {
          response.data.forEach(function(value, index) {
            companyActivityOptions.push(<option value={value.id}>{value.name}</option>);
          });
        }

        this.setState({'companyActivityOptions' : companyActivityOptions});
      }).bind(this));
  }

  render() {
    return (
      <div class="poi-edit mb-3">
        <h5 class="h5 mt-5">Nouveau parent <button class="btn btn-xs btn-warning float-right" onClick={this.props.collection.cancelAddPoi}><Icon.X size="1em" />&nbsp;Annuler</button></h5>
        <Formik
          initialValues={{
            type: '',
            last_name: '',
            first_name: '',
            usage_name: '',
            phone: '',
            primary_email: '',
            company: '',
            company_activity: '',
            company_phone: '',
            company_address: '',
            job_details: '',
            job_type: '',
            job_title: ''
          }}
          onSubmit={(values, { setSubmitting, setFieldError, setStatus }) => {
           API.post('/register/step-2', values)
              .then((function(response) {
                var pois = this.props.collection.state.pois;
                values.id = response.data[0];
                pois.push(values);
                this.props.collection.setState({'pois': pois});
                this.props.collection.cancelAddPoi();
              }).bind(this))
              .catch((function(error) {
                _.forOwn(error.response.data[0], function(value, key) {
                  setFieldError(_.split(key, '.')[1], value);
                });

              }).bind({ setFieldError: setFieldError}))
            ;
          }}
        >
          <Form>
            <div class="form-group">
              <label class="form-control-label">Relation</label>
              <div class="input-group">
                <Field
                  name="type"
                  as="select"
                  className="form-control"
                >
                  <option value="">Veuillez choisir</option>
                  <option value="20">Père</option>
                  <option value="30">Mère</option>
                  <option value="40">Tuteur</option>
                </Field>
              </div>
              <ErrorMessage name="type" render={msg => <div class="text-danger"><Icon.XOctagon size="1em" />&nbsp;{msg}</div>} />
            </div>
            <div class="form-group">
              <label class="form-control-label">Nom</label>
              <div class="input-group">
                <Field
                  name="last_name"
                  as="input"
                  className="form-control"
                  placeholder="ex. Rajaonera"
                />
              </div>
              <ErrorMessage name="last_name" render={msg => <div class="text-danger"><Icon.XOctagon size="1em" />&nbsp;{msg}</div>} />
            </div>
            <div class="form-group">
              <label class="form-control-label">Prénom(s)</label>
              <div class="input-group">
                <Field
                  name="first_name"
                  as="input"
                  className="form-control"
                  placeholder="ex. Hery Christophe"
                />
              </div>
              <ErrorMessage name="first_name" render={msg => <div class="text-danger"><Icon.XOctagon size="1em" />&nbsp;{msg}</div>} />
            </div>
            <div class="form-group">
              <label class="form-control-label">Prénom d'usage</label>
              <div class="input-group">
                <Field
                  name="usage_name"
                  as="input"
                  className="form-control"
                  placeholder="ex. Hery"
                />
              </div>
              <ErrorMessage name="usage_name" render={msg => <div class="text-danger"><Icon.XOctagon size="1em" />&nbsp;{msg}</div>} />
            </div>
            <div class="form-group">
              <label class="form-control-label">Téléphone</label>
              <div class="input-group">
                <Field
                  name="phone"
                  as="input"
                  className="form-control"
                  placeholder="ex. +261 34 05 768 94"
                />
              </div>
              <ErrorMessage name="phone" render={msg => <div class="text-danger"><Icon.XOctagon size="1em" />&nbsp;{msg}</div>} />
            </div>
            <div class="form-group">
              <label class="form-control-label">E-mail</label>
              <div class="input-group">
                <Field
                  name="primary_email"
                  as="input"
                  className="form-control"
                  placeholder="ex. hery@exemple.com"
                />
              </div>
              <ErrorMessage name="primary_email" render={msg => <div class="text-danger"><Icon.XOctagon size="1em" />&nbsp;{msg}</div>} />
            </div>
            <div class="form-group">
              <label class="form-control-label">Situation professionnelle</label>
              <div class="input-group">
                <Field
                  name="job_title"
                  as="select"
                  className="form-control"
                >
                  <option value="">Veuillez choisir</option>
                  {this.state.jobTitleOptions}
                </Field>
              </div>
              <ErrorMessage name="job_title" render={msg => <div class="text-danger"><Icon.XOctagon size="1em" />&nbsp;{msg}</div>} />
            </div>
            <div class="form-group">
              <label class="form-control-label">Poste dans l'organisation</label>
              <div class="input-group">
                <Field
                  name="job_type"
                  as="select"
                  className="form-control"
                >
                  <option value="">Veuillez choisir</option>
                  {this.state.jobTypeOptions}
                </Field>
              </div>
              <ErrorMessage name="job_type" render={msg => <div class="text-danger"><Icon.XOctagon size="1em" />&nbsp;{msg}</div>} />
            </div>
            <div class="form-group">
              <label class="form-control-label">Fonction dans l'organisation</label>
              <div class="input-group">
                <Field
                  name="job_details"
                  as="input"
                  className="form-control"
                  placeholder="ex. Directeur des ventes"
                />
              </div>
              <ErrorMessage name="job_details" render={msg => <div class="text-danger"><Icon.XOctagon size="1em" />&nbsp;{msg}</div>} />
            </div>
            <div class="form-group">
              <label class="form-control-label">Société</label>
              <div class="input-group">
                <Field
                  name="company"
                  as="input"
                  className="form-control"
                  placeholder="ex. STAR"
                />
              </div>
              <ErrorMessage name="company" render={msg => <div class="text-danger"><Icon.XOctagon size="1em" />&nbsp;{msg}</div>} />
            </div>
            <div class="form-group">
              <label class="form-control-label">Domaine d'activité</label>
              <div class="input-group">
                <Field
                  name="company_activity"
                  as="select"
                  className="form-control"
                >
                  <option value="">Veuillez choisir</option>
                  {this.state.companyActivityOptions}
                </Field>
              </div>
              <ErrorMessage name="company_activity" render={msg => <div class="text-danger"><Icon.XOctagon size="1em" />&nbsp;{msg}</div>} />
            </div>
            <div class="form-group">
              <label class="form-control-label">Téléphone société</label>
              <div class="input-group">
                <Field
                  name="company_phone"
                  as="input"
                  className="form-control"
                  placeholder="ex. +261 34 09 827 99"
                />
              </div>
              <ErrorMessage name="company_phone" render={msg => <div class="text-danger"><Icon.XOctagon size="1em" />&nbsp;{msg}</div>} />
            </div>
            <div class="form-group">
              <label class="form-control-label">Adresse société</label>
              <div class="input-group">
                <Field
                  name="company_address"
                  as="input"
                  className="form-control"
                  placeholder="ex. IVN 68A Ankadifotsy 101 Antananarivo"
                />
              </div>
              <ErrorMessage name="company_address" render={msg => <div class="text-danger"><Icon.XOctagon size="1em" />&nbsp;{msg}</div>} />
            </div>
            <button class="btn btn-primary btn-block" type="submit">Valider</button>
          </Form>
        </Formik>
      </div>
    );
  }
}

export default PersonForm;
