import React, { Component } from 'react';

import * as Icon from 'react-feather';

class EditPerson extends Component {
  constructor(props) {
    super(props);

    this.model = props.model;
    this.collection = props.collection;

    this.state = {
      'relation'  : '',
      'last_name' : '',
      'first_name': '',
      'phone': '',
      'email': '',
      'job_type': '',
      'job_title': '',
      'job_description': '',
      'company': '',
      'company_activity': '',
      'company_phone': '',
      'company_address': ''
    };

    this.change = this.change.bind(this);
    this.submitPerson = this.submitPerson.bind(this);
  }

  change(e) {
    this.setState({[e.target.name] : e.target.value});
  }

  submitPerson() {
    this.collection.savePoi(this.state);
    this.collection.cancelAddPoi();
  }

  render() {
    return (
      <div class="poi-edit mb-3">
          <h5 class="h5 mt-5">Nouveau parent <button class="btn btn-xs btn-warning float-right" onClick={this.collection.cancelAddPoi}><Icon.X size="1em" />&nbsp;Annuler</button></h5>
          <div class="form-group">
              <label class="form-control-label">Relation</label>
              <div class="input-group">
                  <select class="form-control" name="relation" onChange={this.change} value={this.state.relation}>
                      <option>Veuillez choisir</option>
                  </select>
              </div>
          </div>
          <div class="form-group">
              <label class="form-control-label">Nom</label>
              <div class="input-group">
                  <input type="text" class="form-control" name="last_name" onChange={this.change} value={this.state.last_name} placeholder="ex. Rajaonera" />
              </div>
          </div>
          <div class="form-group">
              <label class="form-control-label">Prénom(s)</label>
              <div class="input-group">
                  <input type="text" class="form-control" name="first_name" onChange={this.change} value={this.state.first_name} placeholder="ex. Hery Christophe" />
              </div>
          </div>
          <div class="form-group">
              <label class="form-control-label">Téléphone</label>
              <div class="input-group">
                  <input type="text" class="form-control" name="phone" onChange={this.change} value={this.state.phone} placeholder="ex. +261 32 67 098 01" />
              </div>
          </div>
          <div class="form-group">
              <label class="form-control-label">E-mail</label>
              <div class="input-group">
                  <input type="text" class="form-control" name="email" onChange={this.change} value={this.state.email} placeholder="ex. Johary" />
              </div>
          </div>
          <div class="form-group">
              <label class="form-control-label">Situation professionnelle</label>
                  <select class="form-control" name="job_type" onChange={this.change} value={this.state.job_type}>
                      <option>Veuillez choisir</option>
                  </select>
          </div>
          <div class="form-group">
              <label class="form-control-label">Poste dans l'organisation</label>
              <select class="form-control" name="job_title" onChange={this.change} value={this.state.job_title}>
                  <option>Veuillez choisir</option>
              </select>
          </div>
          <div class="form-group">
              <label class="form-control-label">Fonction dans l'organisation</label>
              <div class="input-group">
                  <input type="text" class="form-control" name="job_description" onChange={this.change} value={this.state.job_description} placeholder="ex. Responsable des ventes" />
              </div>
          </div>
          <div class="form-group">
              <label class="form-control-label">Société</label>
              <div class="input-group">
                  <input type="text" class="form-control" name="company" onChange={this.change} value={this.state.company} placeholder="ex. STAR" />
              </div>
          </div>
          <div class="form-group">
              <label class="form-control-label">Domaine d'activité</label>
              <div class="input-group">
                  <select class="form-control" name="company_activity" onChange={this.change} value={this.state.company_activity}>
                      <option>Veuillez choisir</option>
                  </select>
              </div>
          </div>
          <div class="form-group">
              <label class="form-control-label">Téléphone Société</label>
              <div class="input-group">
                  <input type="text" class="form-control" name="company_phone" onChange={this.change} value={this.state.company_phone} placeholder="+261 32 09 898 76" />
              </div>
          </div>
          <div class="form-group">
              <label class="form-control-label">Adresse Société</label>
              <div class="input-group">
                  <input type="text" class="form-control" name="company_address" onChange={this.change} value={this.state.company_address} placeholder="ex. IVN 68A Ankadifotsy, 101 Antananarivo" />
              </div>
          </div>
          <button class="btn btn-primary btn-block" onClick={this.submitPerson}>Enregistrer</button>
      </div>
    );
  }
}

export default EditPerson;
