import React, { Component } from 'react';
import * as Icon from 'react-feather';

class School extends Component {
  constructor(props) {
    super(props);

    this.collection = props.collection;
    this.index = props.index;

    this.editSchool = this.editSchool.bind(this);
  }

  editSchool() {
    this.collection.editSchool(this.index);
  }

  render() {
    return (
      <tr>
          <td>{this.props.model.school_name}</td>
          <td>{this.props.model.level}</td>
          <td>{this.props.model.year}</td>
          <td><a href="#" onClick={this.editSchool} class="btn btn-xs btn-warning"><Icon.Edit size="1em" /></a></td>
      </tr>
    );
  }
}

export default School;
