import React, { Component } from 'react';
import * as Icon from 'react-feather';

class EditSchool extends Component {
  constructor(props) {
    super(props);

    this.model = props.model;
    this.collection = props.collection;
    this.index = props.index;

    if (this.model) {
      this.state = this.model;
      this.index = this.index;
    } else {
      this.state = {
        'school_name' : '',
        'place': '',
        'level': '',
        'year': '',
      };
    }

    this.change = this.change.bind(this);
    this.submitSchool = this.submitSchool.bind(this);
  }

  change(e) {
    this.setState({[e.target.name] : e.target.value});
  }

  submitSchool() {
    this.collection.saveSchool(this.state, this.index);
    this.collection.cancelAddSchool();
  }

  render() {
    return (
      <div class="poi-edit mb-3">
          <h5 class="h5 mt-5">Edition d'une école <button class="btn btn-xs btn-warning float-right" onClick={this.collection.cancelAddSchool}><Icon.X size="1em" />&nbsp;Annuler</button></h5>
          <div class="form-group">
              <label class="form-control-label">Nom de l'école</label>
              <div class="input-group">
                  <input type="text" class="form-control" name="school_name" onChange={this.change} value={this.state.school_name} placeholder="ex. Lycée Saint-François" />
              </div>
          </div>
          <div class="form-group">
              <label class="form-control-label">Lieu</label>
              <div class="input-group">
                  <input type="text" class="form-control" name="place" onChange={this.change} value={this.state.place} placeholder="ex. Antananarivo" />
              </div>
          </div>
          <div class="form-group">
              <label class="form-control-label">Classe</label>
              <div class="input-group">
                  <input type="text" class="form-control" name="level" onChange={this.change} value={this.state.level} placeholder="ex. Terminale" />
              </div>
          </div>
          <div class="form-group">
              <label class="form-control-label">Année</label>
              <div class="input-group">
                  <input type="text" class="form-control" name="year" onChange={this.change} value={this.state.year} placeholder="ex. 2020" />
              </div>
          </div>
          <button class="btn btn-primary btn-block" onClick={this.submitSchool}>Enregistrer</button>
      </div>
    );
  }
}

export default EditSchool;
