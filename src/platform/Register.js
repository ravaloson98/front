import React, { Component } from 'react';
import * as Icon from 'react-feather';

import RegisterStep1Background from '../assets/img/backgrounds/register-step-1.jpg';
import './Register.scss';

import { RegisterStep } from './register-steps/RegisterSteps';

class Register extends Component {
  render() {
    return (
      <div>
          <a href="/" class="btn btn-white btn-icon-only rounded-circle position-absolute zindex-101 left-4 top-4 d-none d-lg-inline-flex" data-toggle="tooltip" data-placement="right" title="Go back">
              <span class="btn-inner--icon">
                  <Icon.ArrowLeft size="1em" />
              </span>
          </a>
          <section>
              <RegisterStep step={this.props.step} />
          </section>
      </div>
    );
  }
}

export default Register;
