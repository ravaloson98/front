import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import * as Icon from 'react-feather';
import API from '../../Api';

class Step1 extends Component {
  constructor(props) {
    super(props);

    this.state = {
      'redirect': null,
      'showPassword' : 'password',
      'countryOptions' : [],
      'interestOptions' : [],
      'applicant': {
        'person' : {
          'last_name'   : '',
          'first_name'  : '',
          'usage_name'  : '',
          'birthdate'  : '',
          'sex'         : '',
          'birth_city'  : '',
          'birth_country' : '',
          'nationality' : '',
          'marital_status' : '',
          'address' : '',
          'secondary_address' : '',
          'mobile_phone' : '',
          'phone' : '',
          'user' : {
            'email' : '',
            'password' : ''
            },
          },
        },
        'errors': [],
        'family_members_at_iscam' : '',
        'interests' : ''
    };

    this.togglePassword = this.togglePassword.bind(this);
    this.change = this.change.bind(this);
    this.changePerson = this.changePerson.bind(this);
    this.changeUser = this.changeUser.bind(this);
    this.submitApplicant = this.submitApplicant.bind(this);
    this.populateCountries = this.populateCountries.bind(this);
    this.populateInterests = this.populateInterests.bind(this);
    this.renderError = this.renderError.bind(this);
    this.hasError = this.hasError.bind(this);

    this.populateCountries();
    this.populateInterests();
  }

  togglePassword() {
    this.setState({'showPassword' : (this.state.showPassword == 'password') ? 'text' : 'password'});
  }

  change(event) {
    var applicant = this.state.applicant;

    if (event.target.name == 'interests') {
      applicant[event.target.name] = [ event.target.value ];
    } else {
      applicant[event.target.name] = event.target.value;
    }

    this.setState({'applicant' : applicant });
  }

  changePerson(event) {
    var applicant = this.state.applicant;

    applicant.person[event.target.name] = event.target.value;

    this.setState({'applicant' : applicant });
  }


  changeUser(event) {
    var applicant = this.state.applicant;

    applicant.person.user[event.target.name] = event.target.value;

    this.setState({'applicant' : applicant });
  }

  submitApplicant() {
    API.post('/register/step-1', this.state.applicant)
      .then((function(response) {
        var applicant = this.state.applicant;
        applicant.apiKey = response.data[0];
        window.apiKey = applicant.apiKey;
        this.setState({ 'applicant' : applicant });
        this.props.registerStep.setState({'step1' : this.state.applicant});
        this.setState({'redirect' : '/register/2'});
      }).bind(this))
      .catch((function(error) {
        this.setState({'errors' : error.response.data[0]});
      }).bind(this))
    ;
  }

  populateCountries() {
    API.get('/list/countries')
      .then((function (response) {
        var countryOptions = [];

        if (Array.isArray(response.data)) {
          response.data.forEach(function(value, index) {
            countryOptions.push(<option value={value.id}>{value.name_fr}</option>);
          });
        }

        this.setState({'countryOptions' : countryOptions});
      }).bind(this));
  }

  populateInterests() {
    API.get('/list/interests')
      .then((function(response) {
        var interestOptions = [];

        if (Array.isArray(response.data)) {
          response.data.forEach(function(value, index) {
            interestOptions.push(<option value={value.id}>{value.name}</option>);
          });
        }

        this.setState({'interestOptions' : interestOptions});
      }).bind(this));
  }

  renderError(e) {
    if (this.hasError(e)) {
      return (<div class="text-danger"><Icon.XOctagon size="1em" />&nbsp;{this.state.errors[e]}</div>);
    }
  }

  hasError(e) {
    var keys = Object.keys(this.state.errors);
    return keys.includes(e);
  }

  render() {
    if (this.state.redirect) {
      return <Redirect to={this.state.redirect} />
    }

    return (
      <div class="register-right-pane container-fluid d-flex flex-column">
          <div class="row align-items-center justify-content-center justify-content-lg-end min-vh-100">
              <div class="scrollable-content col-sm-7 col-lg-6 col-xl-6 py-6 py-lg-0 position-absolute h-100 top-0 right-0">
                  <div class="row justify-content-center">
                      <div class="col-11 col-lg-10 col-xl-10">
                          <div>
                              <div class="mb-5 mt-5">
                                  <h6 class="h3 mb-1">Inscription Concours ISCAM</h6>
                                  <p class="text-muted mb-0">Informations élémentaires</p>
                              </div>
                              <span class="clearfix"></span>
                              <form>
                                  <div class="form-group">
                                      <label class="form-control-label">Nom de famille</label>
                                      <div class="input-group">
                                        <input type="text" class={"form-control".concat(this.hasError('data.relationships[0].last_name') ? ' is-invalid' : '')} name="last_name" onChange={this.changePerson} value={this.state.applicant.person.last_name} placeholder="ex. Rakotomalala" />
                                      </div>
                                      {this.renderError('data.relationships[0].last_name')}
                                  </div>
                                  <div class="form-group">
                                      <label class="form-control-label">Prénom(s)</label>
                                      <div class="input-group">
                                          <input type="text" class={"form-control".concat(this.hasError('data.relationships[0].first_name') ? ' is-invalid' : '')} name="first_name" onChange={this.changePerson} value={this.state.applicant.person.first_name} placeholder="ex. Johary David" />
                                      </div>
                                      {this.renderError('data.relationships[0].first_name')}
                                  </div>
                                  <div class="form-group">
                                      <label class="form-control-label">Prénom d'usage</label>
                                      <div class="input-group">
                                        <input type="text" class={"form-control".concat(this.hasError('data.relationships[0].usage_name') ? ' is-invalid' : '')} name="usage_name" onChange={this.changePerson} value={this.state.applicant.person.usage_name} placeholder="ex. Johary" />
                                      </div>
                                      {this.renderError('data.relationships[0].usage_name')}
                                  </div>
                                  <div class="form-group">
                                      <label class="form-control-label">Date de Naissance</label>
                                      <div class="input-group">
                                        <input type="text" class={"form-control".concat(this.hasError('data.relationships[0].birthdate') ? ' is-invalid' : '')} name="birthdate" onChange={this.changePerson} value={this.state.applicant.person.birthdate} placeholder="ex. 05/12/2002" />
                                      </div>
                                      {this.renderError('data.relationships[0].birthdate')}
                                  </div>
                                  <div class="form-group">
                                      <label class="form-control-label">Genre</label>
                                      <div class="input-group">
                                          <select name="sex" onChange={this.changePerson} value={this.state.applicant.person.sex} class={"form-control".concat(this.hasError('data.relationships[0].sex') ? ' is-invalid' : '')}>
                                              <option>Veuillez choisir</option>
                                              <option value="1">Homme</option>
                                              <option value="0">Femme</option>
                                          </select>
                                      </div>
                                      {this.renderError('data.relationships[0].sex')}
                                  </div>
                                  <div class="form-group">
                                      <label class="form-control-label">Lieu de Naissance</label>
                                      <div class="input-group">
                                          <input type="text" class={"form-control".concat(this.hasError('data.relationships[0].birth_city') ? ' is-invalid' : '')} name="birth_city" onChange={this.changePerson} value={this.state.applicant.person.birth_city} placeholder="ex. Toamasina" />
                                      </div>
                                      {this.renderError('data.relationships[0].birth_city')}
                                  </div>
                                  <div class="form-group">
                                      <label class="form-control-label">Pays de Naissance</label>
                                      <div class="input-group">
                                          <select name="birth_country" onChange={this.changePerson} value={this.state.applicant.person.birth_country} class={"form-control".concat(this.hasError('data.relationships[0].birth_country') ? ' is-invalid' : '')}>
                                              <option>Veuillez choisir</option>
                                              {this.state.countryOptions}
                                          </select>
                                      </div>
                                      {this.renderError('data.relationships[0].birth_country')}
                                  </div>
                                  <div class="form-group">
                                      <label class="form-control-label">Nationalité</label>
                                      <div class="input-group">
                                          <select name="nationality" onChange={this.changePerson} value={this.state.applicant.person.nationality} class={"form-control".concat(this.hasError('data.relationships[0].nationality') ? ' is-invalid' : '')}>
                                              <option>Veuillez choisir</option>
                                              {this.state.countryOptions}
                                          </select>
                                      </div>
                                      {this.renderError('data.relationships[0].nationality')}
                                  </div>
                                  <div class="form-group">
                                      <label class="form-control-label">Situation de famille</label>
                                      <div class="input-group">
                                          <select name="marital_status" onChange={this.changePerson} value={this.state.applicant.person.marital_status} class={"form-control".concat(this.hasError('data.relationships[0].marital_status') ? ' is-invalid' : '')}>
                                              <option>Veuillez choisir</option>
                                              <option value="10">Célibataire</option>
                                              <option value="20">Marié</option>
                                              <option value="30">Divorcé</option>
                                              <option value="40">Veuf</option>
                                          </select>
                                      </div>
                                      {this.renderError('data.relationships[0].marital_status')}
                                  </div>
                                  <div class="form-group">
                                      <label class="form-control-label">Adresse principale</label>
                                      <div class="input-group">
                                          <input type="text" class={"form-control".concat(this.hasError('data.relationships[0].address') ? ' is-invalid' : '')} name="address" onChange={this.changePerson} value={this.state.applicant.person.address} placeholder="ex. IVN 68A Ankadifotsy, 101 Antananarivo" />
                                      </div>
                                      {this.renderError('data.relationships[0].address')}
                                  </div>
                                  <div class="form-group">
                                      <label class="form-control-label">Adresse secondaire</label>
                                      <div class="input-group">
                                          <input type="text" class={"form-control".concat(this.hasError('data.relationships[0].secondary_adress') ? ' is-invalid' : '')} name="secondary_address" onChange={this.changePerson} value={this.state.applicant.person.secondary_address} placeholder="ex. IVN 68A Ankadifotsy, 101 Antananarivo" />
                                      </div>
                                      {this.renderError('data.relationships[0].secondary_Address')}
                                  </div>
                                  <div class="form-group">
                                      <label class="form-control-label">Téléphone mobile</label>
                                      <div class="input-group">
                                          <input type="text" class={"form-control".concat(this.hasError('data.relationships[0].mobile_phone') ? ' is-invalid' : '')} name="mobile_phone" onChange={this.changePerson} value={this.state.applicant.personmobile_phone} placeholder="ex. +261 34 22 808 10" />
                                      </div>
                                      {this.renderError('data.relationships[0].mobile_phone')}
                                  </div>
                                  <div class="form-group">
                                      <label class="form-control-label">Téléphone secondaire</label>
                                      <div class="input-group">
                                          <input type="text" class={"form-control".concat(this.hasError('data.relationships[0].phone') ? ' is-invalid' : '')} name="phone" onChange={this.changePerson} value={this.state.applicant.person.phone} placeholder="ex. +261 32 09 765 01" />
                                      </div>
                                      {this.renderError('data.relationships[0].phone')}
                                  </div>
                                  <div class="form-group">
                                      <label class="form-control-label">Adresse e-mail</label>
                                      <div class="input-group">
                                          <input type="email" class={"form-control".concat(this.hasError('data.relationships[0].user.email') ? ' is-invalid' : '')} name="email" onChange={this.changeUser} value={this.state.applicant.person.user.email} placeholder="ex. johary.rakotomalala@exemple.mg" />
                                      </div>
                                      {this.renderError('data.relationships[0].user.email')}
                                  </div>
                                  <div class="form-group mb-4">
                                      <div class="d-flex align-items-center justify-content-between">
                                          <div>
                                              <label class="form-control-label">Mot de passe</label>
                                          </div>
                                          <div class="mb-2">
                                              <a class="small text-muted text-underline--dashed border-primary" onClick={this.togglePassword}>Voir le mot de passe</a>
                                          </div>
                                      </div>
                                      <div class="input-group">
                                          <input type={this.state.showPassword} class={"form-control".concat(this.hasError('data.relationships[0].user.password') ? ' is-invalid' : '')} name="password" onChange={this.changeUser} value={this.state.applicant.person.user.password} placeholder="********" />
                                      </div>
                                      {this.renderError('data.relationships[0].user.password')}
                                  </div>
                                  <div class="form-group">
                                      <label class="form-control-label">Nombre de membres de la famille à l'ISCAM</label>
                                      <div class="input-group">
                                          <input type="text" class={"form-control".concat(this.hasError('data.family_members_at_iscam') ? ' is-invalid' : '')} name="family_members_at_iscam" onChange={this.change} value={this.state.applicant.family_members_at_iscam} placeholder="ex. 3" />
                                      </div>
                                      {this.renderError('data.family_members_at_iscam')}
                                  </div>
                                  <div class="form-group">
                                      <label class="form-control-label">Centre d'intérêt principal</label>
                                      <div class="input-group">
                                          <select name="interests" onChange={this.change} value={this.state.applicant.interests} class={"form-control".concat(this.hasError('data.interests') ? ' is-invalid' : '')}>
                                              <option>Veuillez choisir</option>
                                              {this.state.interestOptions}
                                          </select>
                                      </div>
                                      {this.renderError('data.interests')}
                                  </div>
                                  <div class="mt-4 mb-4">
                                      <button type="button" onClick={this.submitApplicant} class="btn btn-block btn-primary">Valider</button>
                                  </div>
                              </form>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
    );
  }
}

export default Step1;
