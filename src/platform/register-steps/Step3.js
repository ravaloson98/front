import React, { Component } from 'react';
import * as Icon from 'react-feather';

import { School, SchoolForm } from '../../components/Components';

class Step3 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      'baseDiplomaType' : 0,
      'schoolEdit' : false,
      'schools' : []
    };

    this.changeBaseDiplomaType = this.changeBaseDiplomaType.bind(this);
    this.addSchool = this.addSchool.bind(this);
    this.cancelAddSchool = this.cancelAddSchool.bind(this);
    this.saveSchool= this.saveSchool.bind(this);
    this.renderEditSchool = this.renderEditSchool.bind(this);
    this.renderSchools = this.renderSchools.bind(this);
  }

  changeBaseDiplomaType(event) {
    this.setState({'baseDiplomaType' : event.target.value });
  }

  addSchool() {
    this.setState({'schoolEdit' : true});
  }

  cancelAddSchool() {
    this.setState({'schoolEdit' : false});
  }

  saveSchool(school, index) {
    var schools = this.state.schools;

    if (index != undefined) {
      schools[index] = school;
    } else {
      schools.push(school)
    }

    this.setState({'schools': schools});
  }

  editSchool(index) {
    this.setState({'schoolEdit' : index});
  }

  renderEditSchool() {
    if (this.state.schoolEdit === true) {
      return <SchoolForm collection={this} />;
    } else if (typeof(this.state.schoolEdit) == 'number') {
      return <SchoolForm collection={this} index={this.state.schoolEdit} model={this.state.schools[this.state.schoolEdit]} />
    } else {
      return null;
    }
  }

  renderSchools() {
    var schools = [];

    this.state.schools.forEach(function(item, index) {
      schools.push(<School collection={this} model={item} index={index} />);
    }, this);

    if (this.state.schools.length == '0') {
      schools.push(<tr><td class="text-center" colspan="4">Veuillez ajouter 3 années pour continuer&nbsp;&nbsp;<button class="btn btn-xs btn-primary" onClick={this.addSchool}><Icon.Plus size="1em" />&nbsp;Ajouter</button></td></tr>);
    } else if (this.state.schools.length == '1') {
      schools.push(<tr><td class="text-center" colspan="4">Veuillez ajouter 2 années pour continuer&nbsp;&nbsp;<button class="btn btn-xs btn-primary" onClick={this.addSchool}><Icon.Plus size="1em" />&nbsp;Ajouter</button></td></tr>);

    } else if (this.state.schools.length == '2') {
      schools.push(<tr><td class="text-center" colspan="4">Veuillez ajouter une année pour continuer&nbsp;&nbsp;<button class="btn btn-xs btn-primary" onClick={this.addSchool}><Icon.Plus size="1em" />&nbsp;Ajouter</button></td></tr>);

    }

    return schools;
  }

  renderBaseDiploma() {
    switch (this.state.baseDiplomaType) {
      case '1':
        return (
          <div>
              <div class="form-group">
                  <label class="form-control-label">Série Baccalauréat</label>
                  <div class="input-group">
                      <select class="form-control" id="input-name">
                        <option>Veuillez choisir</option>
                      </select>
                  </div>
              </div>
              <div class="form-group">
                  <label class="form-control-label">Mention</label>
                  <div class="input-group">
                      <select class="form-control" id="input-name">
                          <option>Veuillez choisir</option>
                      </select>
                  </div>
              </div>
              <div class="form-group">
                  <label class="form-control-label">Moyenne Baccalauréat</label>
                  <div class="input-group">
                      <input type="text" class="form-control form-control-prepend" id="input-name" placeholder="ex. Johary" />
                  </div>
              </div>
              <div class="form-group">
                  <label class="form-control-label">Année d'obtention</label>
                  <div class="input-group">
                      <input type="text" class="form-control form-control-prepend" id="input-name" placeholder="ex. Johary" />
                  </div>
              </div>
              <div class="form-group">
                  <label class="form-control-label">Lieu d'obtention</label>
                  <div class="input-group">
                      <input type="text" class="form-control form-control-prepend" id="input-name" placeholder="ex. Johary" />
                  </div>
              </div>
          </div>
        );
      case '2':
        return (
          <div>
              <div class="form-group">
                  <label class="form-control-label">EBRW</label>
                  <div class="input-group">
                      <input type="text" class="form-control form-control-prepend" id="input-name" placeholder="ex. Johary" />
                  </div>
              </div>
              <div class="form-group">
                  <label class="form-control-label">Maths</label>
                  <div class="input-group">
                      <input type="text" class="form-control form-control-prepend" id="input-name" placeholder="ex. Johary" />
                  </div>
              </div>
              <div class="form-group">
                  <label class="form-control-label">Année d'obtention</label>
                  <div class="input-group">
                      <input type="text" class="form-control form-control-prepend" id="input-name" placeholder="ex. Johary" />
                  </div>
              </div>
              <div class="form-group">
                  <label class="form-control-label">Lieu d'obtention</label>
                  <div class="input-group">
                      <input type="text" class="form-control form-control-prepend" id="input-name" placeholder="ex. Johary" />
                  </div>
              </div>
          </div>
        );
      default:
        return null;
    }
  }

  renderForm() {
    if (this.state.schoolEdit == true || typeof(this.state.schoolEdit) == 'number') {
      return null;
    }

    return(
      <form>
          <div class="form-group">
              <label class="form-control-label">Année académique</label>
              <div class="input-group">
                  <select class="form-control" id="input-name">
                      <option>2020-2021</option>
                  </select>
              </div>
          </div>
          <div class="form-group">
              <label class="form-control-label">Entrée en</label>
              <div class="input-group">
                  <select class="form-control" id="input-name">
                      <option>Veuillez choisir</option>
                  </select>
              </div>
          </div>
          <div class="form-group">
              <label class="form-control-label">Parcours envisagé</label>
              <div class="input-group">
                  <select class="form-control" id="input-name">
                      <option>Veuillez choisir</option>
                  </select>
              </div>
          </div>
          <div class="form-group">
              <label class="form-control-label">Session de concours</label>
              <div class="input-group">
                  <select class="form-control" id="input-name">
                      <option>Veuillez choisir</option>
                  </select>
              </div>
          </div>
          <label class="form-control-label">Veuillez renseigner vos 3 dernières années scolaires</label>
          <table class="table">
              <thead>
                  <th>Ecole</th>
                  <th>Classe</th>
                  <th>Année</th>
                  <th>Modifier</th>
              </thead>
              {this.renderSchools()}
          </table>
          <div class="form-group">
              <label class="form-control-label">Système scolaire</label>
              <div class="input-group">
                  <select class="form-control" id="input-name" onChange={this.changeBaseDiplomaType} value={this.state.baseDiplomatype}>
                      <option value="0">Veuillez choisir</option>
                      <option value="1">Baccalauréat</option>
                      <option value="2">SAT</option>
                  </select>
              </div>
          </div>
          {this.renderBaseDiploma()}
      </form>
    );
  }

  render() {
    return (
      <div class="register-right-pane container-fluid d-flex flex-column">
          <div class="row align-items-center justify-content-center justify-content-lg-end min-vh-100">
              <div class="scrollable-content col-sm-7 col-lg-6 col-xl-6 py-6 py-lg-0 position-absolute h-100 top-0 right-0">
                  <div class="row justify-content-center">
                      <div class="col-11 col-lg-10 col-xl-10">
                          <div>
                              <div class="mt-5 mb-5">
                                  <h6 class="h3 mb-1">Inscription Concours ISCAM</h6>
                                  <p class="text-muted mb-0">Scolarité</p>
                              </div>
                              <div class="clearfix"></div>
                              {this.renderForm()}
                              {this.renderEditSchool()}
                              <div class={'mt-4 mb-4'.concat(typeof(this.state.schoolEdit) == 'number' || this.state.schoolEdit ? ' disabled' : '')}>
                                  <button type="button" class="btn btn-block btn-primary">Valider</button>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
    );
  }
}

export default Step3;
