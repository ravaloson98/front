import React, { Component } from 'react';
import { Redirect } from 'react-router';
import * as Icon from 'react-feather';
import $ from 'jquery';
import _ from 'lodash';
import API from '../../Api';

import { Person, EditPerson, PersonForm } from '../../components/Components';

class Step2 extends Component {
  constructor(props) {
    super(props);

    if (props.registerStep.state.step1 === null) {
//this.state = {'redirect' : '/register/1'};

//      return;
    }

    this.state = {
      'pois' : [],
      'poiEdit' : false,
    };

    this.addPoi = this.addPoi.bind(this);
    this.renderPersonsOfInterest = this.renderPersonsOfInterest.bind(this);
    this.renderEditPerson = this.renderEditPerson.bind(this);
    this.cancelAddPoi = this.cancelAddPoi.bind(this);
    this.deletePoi = this.deletePoi.bind(this);
  }

  componentDidMount() {
    if (_.has(window, 'apiKey')) {
      API.defaults.headers.common['X-AUTH-TOKEN'] = window.apiKey;
    }
  }

  addPoi() {
    this.setState({'poiEdit' : true});
  }

  cancelAddPoi() {
    this.setState({'poiEdit' : false});
  }

  savePoi(poi) {
    var pois = this.state.pois;
    pois.push(poi)
    this.setState({'pois': pois});
  }

  deletePoi(index) {
    API.delete('/person/' + this.state.pois[index].id)
      .then((function (response) {
        var state = this.state.pois;
        state.splice(index, 1);
        this.setState({'pois' : state});
      }).bind(this));
  }

  renderEditPerson() {
    if (this.state.poiEdit) {
      return <PersonForm collection={this}/>;
    } else {
      return null;
    }
  }

  renderPersonsOfInterest() {
    if (this.state.pois.length < 1 && this.state.poiEdit == false) {
      return (
        <div class="empty-poi">Vous devez ajouter au moins un parent ou tuteur pour pouvoir continuer.</div>
      );
    }

    var pois = [];

    this.state.pois.forEach(function(item, index) {
      pois.push(<Person collection={this} model={item} key={index} index={index} />);
    }, this);

    return pois;
  }

  render() {
    if (this.state.redirect) {
      return <Redirect to={this.state.redirect} />
    }

    return (
      <div class="register-right-pane container-fluid d-flex flex-column">
          <div class="row align-items-center justify-content-center justify-content-lg-end min-vh-100">
              <div class="scrollable-content col-sm-7 col-lg-6 col-xl-6 py-6 py-lg-0 position-absolute h-100 top-0 right-0">
                  <div class="row justify-content-center">
                      <div class="col-11 col-lg-10 col-xl-10">
                          <div>
                              <div class="mt-5 mb-5">
                                  <h6 class="h3 mb-1">Inscription Concours ISCAM</h6>
                                  <p class="text-muted mb-0">Informations de filiation</p>
                              </div>
                              <div class="clearfix"></div>
                              <div class="poi-container">
                                  <h5 class={'h5'.concat(this.state.pois.length == '0' ? ' disabled' : '')}>Parents ajoutés</h5>
                                  {this.renderPersonsOfInterest()}
                                  {this.renderEditPerson()}
                              </div>
                              <div class={'poi-add text-right mt-5'.concat(this.state.poiEdit ? ' disabled' : '')}>
                                  <button class="btn btn-block btn-primary" onClick={this.addPoi}><Icon.UserPlus size="1em" />&nbsp;Ajouter un parent</button>
                              </div>
                              <div class={'mt-4'.concat(this.state.poiEdit ? ' disabled' : '')}>
                                  <button type="button" class="btn btn-block btn-primary">Valider</button>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
    );
  }
}

export default Step2;
