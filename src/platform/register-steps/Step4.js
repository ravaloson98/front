import React, { Component } from 'react';

class Step4 extends Component {
  render() {
    return (
      <div class="register-right-pane container-fluid d-flex flex-column">
          <div class="row align-items-center justify-content-center justify-content-lg-end min-vh-100">
              <div class="scrollable-content col-sm-7 col-lg-6 col-xl-6 py-6 py-lg-0 position-absolute h-100 top-0 right-0">
                  <div class="row justify-content-center">
                      <div class="col-11 col-lg-10 col-xl-10">
                          <div>
                              <div class="mt-5 mb-5">
                                  <h6 class="h3 mb-1">Inscription Concours ISCAM</h6>
                                  <p class="text-muted mb-0">Documents justificatifs nécessaires</p>
                              </div>
                              <span class="clearfix"></span>
                              <p>
                                  Nous avons bien pris en compte les informations que vous avez saisi lors des étapes précédentes.<br />
                                  Merci de préparer les documents suivants afin de finaliser votre inscription :
                              </p>
                              <ul>
                                  <li>CNI</li>
                                  <li>Diplome</li>
                              </ul>
                              <p>Le service de mise en ligne de doucments sera ouvert le Mercredi 17 juin. Un e-mail vous sera envoyé dès son ouverture</p>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
    );
  }
}

export default Step4;
