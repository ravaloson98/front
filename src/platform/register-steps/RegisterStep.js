import React, { Component } from 'react';

import RegisterStep1Background from '../../assets/img/backgrounds/register-step-1.jpg';

import { Step1, Step2, Step3, Step4 } from './RegisterSteps';

class RegisterStep extends Component {
  constructor(props) {
    super(props);

    this.state = {
      'step1' : null,
      'step2' : null,
      'step3' : null
    };

    this.renderStep = this.renderStep.bind(this);
  }

  renderStep(step) {
    switch (step) {
      case '1':
        return <Step1 registerStep={this} />
      case '2':
        return <Step2 registerStep={this} />
      case '3':
        return <Step3 registerStep={this} />
      case '4':
        return <Step4 registerStep={this} />
      default:
        return <Step1 registerStep={this} />
    }
  }

  renderBackground(step) {
    switch (step) {
      case '1':
        this.leftPane = {
          'background'  : RegisterStep1Background,
          'title'       : 'Étape 1 sur 4',
          'description' : 'Veuillez remplir vos informations personnelles. Celles-ci sont la base de votre dossier académique.'
        };
        break;
      case '2':
        this.leftPane = {
          'background'  : RegisterStep1Background,
          'title'       : 'Étape 2 sur 4',
          'description' : 'Veuillez remplir vos informations de filiation. Celles-ci sont requises pour compléter votre dossier.'
        };
        break;
      case '3':
        this.leftPane = {
          'background'  : RegisterStep1Background,
          'title'       : 'Étape 3 sur 4',
          'description' : 'Veuillez remplir vos informations scolaires. Ces informations nous permettront d\'évaluer votre dossier et vous orienter vers le bon cursus.'
        };
        break;
      case '4':
        this.leftPane = {
          'background'  : RegisterStep1Background,
          'title'       : 'Étape 4 sur 4',
          'description' : 'Veuillez nous envoyer tous vos documents justificatifs, scannés, afin de finaliser votre dossier.'
        };
        break;
      default:
        this.leftPane = {
          'background'  : RegisterStep1Background,
          'title'       : 'Étape 1 sur 4',
          'description' : 'Veuillez remplir vos informations personnelles. Celles-ci sont la base de votre dossier académique.'
        };
    }
  }

  renderLeftPane(step) {
    this.renderBackground(step);

    return (
      <div class="register-left-pane bg-primary position-absolute h-100 top-0 left-0 zindex-100 col-lg-6 col-xl-6 zindex-100 d-none d-lg-flex flex-column justify-content-end" data-bg-size="cover" data-bg-position="center">
          <img src={this.leftPane.background} alt="Image" class="img-as-bg" / >
          <div class="row position-relative zindex-110">
              <div class="step-title">
                  <div class="col-md-8 text-center mx-auto">
                      <h5 class="h5 text-white mt-3">{this.leftPane.title}</h5>
                      <p class="text-white opacity-8">{this.leftPane.description}</p>
                  </div>
              </div>
          </div>
      </div>
    );
  }

  render() {
    return [
      this.renderLeftPane(this.props.step), this.renderStep(this.props.step)
    ];
  }
}

export default RegisterStep;
