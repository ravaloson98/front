import axios from 'axios';
import _ from 'lodash';

export default axios.create({
  baseURL: 'http://api.iscam.localhost',
  headers: _.has(window, 'apiKey') ? { 'X-AUTH-TOKEN': window.apiKey } : {}
});
